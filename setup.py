from distutils.core import setup
from setuptools import find_packages

setup(name='wsserver',
      version='0.1',
      description='websocket server for crowcloud',
      author='timothys@crow.co.il',
      packages=find_packages(),
      entry_points={'console_scripts': [
          'wsserver=wsserver.__init__:main',
      ]})

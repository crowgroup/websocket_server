aiohttp==3.7.4.post0
aiokafka==0.7.0
async-timeout==3.0.1
attrs==20.3.0
chardet==4.0.0
idna==3.1
kafka-python==2.0.2
multidict==5.1.0
sockjs==0.11.0
typing-extensions==3.7.4.3
ujson==4.0.2
uvloop==0.15.2
yarl==1.6.3
asyncclick==7.1.2.3
orjson==3.5.2
aioredis==2.0.1

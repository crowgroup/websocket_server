import aiohttp
import asyncio
import asyncclick as click
import json

AUTH_URL = ''
URL = 'ws://dev-server.crowcloud.xyz:9880/sockjs/websocket'


async def auth(username, password):
    pass


@click.command()
@click.option("--url", help="websocket server url", default=URL)
@click.option("--token", help="auth bearer token", required=True)
@click.option("--panel", type=click.INT, help="panel id for subscription", required=True)
async def main(url, token, panel):
    async with aiohttp.ClientSession() as session:
        async with session.ws_connect(url) as ws:
            subscribed = False
            await ws.send_json({'type': 'authentication', 'value': token})

            while True:
                msg = await ws.receive()
                print(msg)
                if msg.type == aiohttp.WSMsgType.TEXT:
                    data = json.loads(msg.data)
                    if 'type' in data:
                        if data['type'] in ('raw_event', 'info'):
                            print(data)
                    if not subscribed:
                        await ws.send_json({'type': 'subscribe', 'value': panel})
                        subscribed = True
                elif msg.type in (aiohttp.WSMsgType.CLOSED, aiohttp.WSMsgType.CLOSE):
                    print('closed')
                    break
                elif msg.type == aiohttp.WSMsgType.ERROR:
                    print('error')
                    break


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())

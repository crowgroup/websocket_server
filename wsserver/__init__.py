from aiohttp import web
from wsserver import settings
from aiokafka import AIOKafkaConsumer
from collections import namedtuple
import asyncio
import orjson
import uvloop
import aioredis
import async_timeout

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
import aiohttp
import ujson as json
import logging
import sockjs
from wsserver.helpers import get_config
from datetime import timedelta

OK = 'OK'
ERROR = 'ERROR'
UNKNOWN_CMD = 'unknown command'
NOT_AUTHENTICATED = 'not authenticated'
ALREADY_AUTHENTICATED = 'already authenticated'
INVALID_TOKEN = 'invalid token'
MALFORMED_DATA = 'malformed data received'
ALREADY_SUBSCRIBED = 'already subscribed'
NO_PERMISSION = 'no permission'

REST_ERRORS = {401: INVALID_TOKEN, 403: NO_PERMISSION}

log = logging.getLogger(__name__)
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter('%(asctime)s | %(name)s | %(levelname)s | %(message)s'))
log.addHandler(handler)
log.setLevel(logging.DEBUG)

Client = namedtuple('Client', ['token', 'panels'])
SESSION_MANAGER = None


def nested_get(obj, path):
    tmp = obj
    for e in path:
        tmp = tmp.get(e, {})
    return tmp


def kafka_deserializer(serialized):
    return orjson.loads(serialized)


def create_kafka_consumer(loop):
    return AIOKafkaConsumer(*settings.CONSUMER_TOPICS.keys(),
                            value_deserializer=kafka_deserializer,
                            loop=loop,
                            group_id=settings.GROUP_ID,
                            bootstrap_servers=settings.CONSUMER_BOOTSTRAP_SERVERS)


async def process_raw_event(data):
    cid = str(data['cid'])
    params = {'par1': data['param1'], 'par2': data['param2']}
    cids = get_config()
    event = cids.get(cid, None)
    msgs = []
    sections = []
    indexes = []
    if event:
        for param in ('par1', 'par2'):
            meta_key = f'{param}_meta'
            meta = event.get(meta_key, None)
            sections.append(event[param])
            indexes.append(params[param])
            if meta:
                for k, v in zip(meta['keys'], meta['values']):
                    msgs.append({'key': k, 'value': v, 'sections': sections, 'indexes': indexes})
    return msgs


async def listen_kafka(loop, session_manager):
    consumer = create_kafka_consumer(loop)
    await consumer.start()

    try:
        async for message in consumer:
            topic = settings.CONSUMER_TOPICS.get(message.topic, None)
            if topic:
                try:
                    panel_mac = nested_get(message.value, topic['key'])
                    if panel_mac:
                        panel_mac = panel_mac.lower()
                        sockets = session_manager.get_sockets(panel_mac)
                        if sockets:
                            if message.topic == 'crow_report_raw_events':
                                processed_messages = await process_raw_event(message.value)
                            else:
                                processed_messages = [message.value]
                            for sock in sockets:
                                for msg in processed_messages:
                                    sock.send(orjson.dumps({'type': topic['type'], 'data': msg}).decode("utf-8"))
                except Exception as exc:
                    log.warning(f'send message error: {exc}', exc_info=True)
    except Exception as exc:
        log.warning(f'listen kafka error: {exc}', exc_info=True)


async def ask_rest(token, method, path):
    async with aiohttp.ClientSession() as session:
        try:
            url = f'{settings.REST_URL}{path}'
            headers = {'Authorization': f'Bearer {token}'}
            func = getattr(session, method.lower())
            async with func(url, headers=headers, timeout=settings.HTTP_TIMEOUT) as response:
                if response.status != 200:
                    raise CrowError(REST_ERRORS.get(response.status, f'error {response.status}'))
                return await response.json()
        except asyncio.TimeoutError:
            log.warning(f'{method} {path} reached timeout')
            raise CrowError('timeout in auth')


def send_error(sock, description):
    sock.send(orjson.dumps({'status': ERROR, 'description': description}).decode("utf-8"))


def send_ok(sock):
    sock.send(orjson.dumps({'status': OK}).decode("utf-8"))


class CrowError(Exception):
    pass


class CrowSessionManager(sockjs.SessionManager):
    def __init__(self, *args, **kwargs):
        self.clients = {}
        self.panels = {}

        super().__init__('events', *args, **kwargs)

    async def authenticate(self, session, token):
        await ask_rest(token, 'get', '/auth/user/')
        if session not in self.clients:
            self.clients[session] = Client(token, set())
        else:
            raise CrowError('session already authenticated')

    async def subscribe(self, session, sub_key):
        client = self.clients.get(session, None)
        if client:
            reply = await ask_rest(client.token, 'get', '/panels/{}/'.format(sub_key))
            panel_mac = reply.get('mac', None)
            if panel_mac is None:
                raise CrowError('No mac field in REST API reply')
            if panel_mac not in client.panels:
                self.panels.setdefault(panel_mac, set())
                client.panels.add(panel_mac)
                self.panels[panel_mac].add(session)
        else:
            raise CrowError(NOT_AUTHENTICATED)

    def remove_session(self, session):
        client = self.clients.pop(session, None)
        if client:
            for panel_mac in client.panels:
                try:
                    self.panels[panel_mac].remove(session)
                except KeyError:
                    pass

    def get_sockets(self, panel_mac):
        return self.panels.get(panel_mac, [])

    def num_clients(self):
        return len(self.clients)


async def handle_message(msg, session):
    try:
        try:
            cmd = orjson.loads(msg.data)
            cmd_type = cmd['type']
            cmd_value = cmd['value']
        except (KeyError, ValueError):
            raise CrowError(MALFORMED_DATA)
        if cmd_type == 'authentication':
            await session.manager.authenticate(session, cmd_value)
            send_ok(session)
            log.info(f'session {session} pass authentication')
        elif cmd_type == 'subscribe':
            await session.manager.subscribe(session, cmd_value)
            send_ok(session)
            log.info(f'session {session} subscribed to panel {cmd_value}')
        elif cmd_type == 'ping':
            send_ok(session)
        else:
            raise CrowError(UNKNOWN_CMD)
    except CrowError as exc:
        log.warning(exc)
        send_error(session, str(exc))


async def sockjs_handler(msg, session):
    if msg.tp == sockjs.MSG_OPEN:
        log.info(f'new session {session} opened')
    elif msg.tp == sockjs.MSG_MESSAGE:
        await handle_message(msg, session)
    elif msg.tp == sockjs.MSG_CLOSED:
        log.info(f'session {session} closed')
        # Because at this point sockjs already set attribute manager to None
        SESSION_MANAGER.remove_session(session)


async def monitor(session_manager):
    while True:
        log.debug(f'currently we have {session_manager.num_clients()} connected client(s)')
        await asyncio.sleep(settings.MONITOR_INTERVAL)


async def listen_redis(loop, session_manager):
    redis = aioredis.Redis(
        host=settings.REDIS_SERVER, max_connections=10, decode_responses=True, socket_timeout=11, socket_connect_timeout=5
    )
    psub = redis.pubsub()

    async def reader(channel: aioredis.client.PubSub, session_manager):
        while True:
            try:
                async with async_timeout.timeout(10):
                    message = await channel.get_message(ignore_subscribe_messages=True)
                    if message is not None:
                        panel_mac = message['channel'].split(':')[-1]
                        if panel_mac:
                            panel_mac = panel_mac.lower()
                            sockets = session_manager.get_sockets(panel_mac)
                            if sockets:
                                processed_message = '{"type": "mediator_data", "data": ' + message['data'] + '}'
                                for sock in sockets:
                                    sock.send(processed_message)
                    await asyncio.sleep(0.01)
            except asyncio.TimeoutError:
                pass
            except Exception as e:
                print(e)

    async with psub as p:
        await p.psubscribe(settings.REDIS_CHANNEL)
        log.info(f'redis psubscribed to {settings.REDIS_CHANNEL}')
        await reader(p, session_manager)  # wait for reader to complete
        await p.punsubscribe(settings.REDIS_CHANNEL)

    # closing all open connections
    await psub.close()

    print('redis connection closed')

def make_app(loop):
    global SESSION_MANAGER
    app = web.Application(loop=loop)
    session_manager = CrowSessionManager(app, sockjs_handler, heartbeat=5, timeout=timedelta(seconds=60))
    SESSION_MANAGER = session_manager
    asyncio.ensure_future(monitor(session_manager))
    asyncio.ensure_future(listen_kafka(loop, session_manager))
    asyncio.ensure_future(listen_redis(loop, session_manager))
    sockjs.add_endpoint(app, handler=sockjs_handler, manager=session_manager, name='events', prefix='/sockjs/')
    return app


def main():
    loop = asyncio.get_event_loop()
    app = make_app(loop)
    web.run_app(app)


if __name__ == '__main__':
    main()

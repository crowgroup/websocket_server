import os

CONSUMER_BOOTSTRAP_SERVERS = os.getenv('CONSUMER_BOOTSTRAP_SERVERS', 'kafka-1.crowcloud.xyz').split(',')
CONSUMER_TOPICS = {
    'crow_report_raw_events': {
        'type': 'raw_event',
        'key': ['control_panel']
    },
    'crow_zone_monitoring': {
        'type': 'zone_monitoring',
        'key': ['control_panel']
    },
    'crow_ready_event': {
        'type': 'event',
        'key': ['control_panel_info', 'mac']
    },
    'crow_ready_picture': {
        'type': 'picture',
        'key': ['control_panel_info', 'mac']
    },
    'crow_dect_enricher': {
        'type': 'info',
        'key': ['_id', 'control_panel']
    }
}
REST_URL = os.getenv('REST_URL', 'https://api.crowcloud.xyz')
HTTP_TIMEOUT = int(os.getenv('HTTP_TIMEOUT', 10))
MONITOR_INTERVAL = int(os.getenv('MONITOR_INTERVAL', 10))
CID_FILE = 'cid_info.json'
CID_OVERRIDE_FILE = 'cid_info_override.json'
GROUP_ID = os.getenv('CONSUMER_GROUP_ID', None)
REDIS_SERVER = os.getenv('REDIS_SERVER', 'mediator.k.crowcloud.xyz')
REDIS_CHANNEL = os.getenv('REDIS_CHANNEL', 'panel:live:events:*')

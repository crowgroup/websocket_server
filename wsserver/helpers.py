import ujson as json
from wsserver.settings import CID_FILE, CID_OVERRIDE_FILE


def reader(filename, filename_override):
    data = None

    def func():
        nonlocal data
        if data is None:
            with open(filename) as fin:
                temp = json.load(fin)
            temp = {k: v[0] for k, v in temp.items()}
            with open(filename_override) as fin:
                data = json.load(fin)
                for k, v in data.items():
                    data[k] |= temp[k]
        return data

    return func


get_config = reader(CID_FILE, CID_OVERRIDE_FILE)
# a = get_config()
# print(a)
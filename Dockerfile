FROM python:3.9.1-slim AS compile-image
RUN apt-get update
RUN apt-get install -y --no-install-recommends build-essential gcc curl
RUN curl https://getmic.ro | bash

COPY requirements.txt .
RUN pip install --user -r requirements.txt


FROM python:3.9.1-slim AS build-image
COPY --from=compile-image /root/.local /root/.local
COPY --from=compile-image /micro /bin

LABEL Description="Image for WebSocket Server"
COPY / /crow

ENV PATH=/root/.local/bin:$PATH
RUN mkdir /root/.aws && echo '[default]' > /root/.aws/config && echo  'region=eu-central-1' >> /root/.aws/config
WORKDIR /crow
EXPOSE 8080
CMD ["/usr/local/bin/python", "-m","wsserver"]

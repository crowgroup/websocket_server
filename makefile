PWD := ${shell pwd}
GITBRANCH ?= ${shell git rev-parse --abbrev-ref HEAD}
DOCKER_FILE := Dockerfile
ifneq (${GITBRANCH}, master)
else
    GITBRANCH := latest
endif


all: build

build:
	docker build -t crowcloud/wsserver:${GITBRANCH} -f ${DOCKER_FILE} .

buildx:
	docker buildx build --platform linux/amd64 -t crowcloud/wsserver:${GITBRANCH} -f ${DOCKER_FILE} .

push:
	docker push crowcloud/wsserver:${GITBRANCH}

Intro
=====
Websocket server it is gateway between kafka and websocket clients. It implements protocol for authentication and
events subscription for specific control panels and mechanism for routing messages received from kafka to related clients.
Once you passed authentication and subscribed to some panels, your client will receive events from
these panels.

Server support both sockjs and raw websocket protocol.

To access sockjs version, open URL:

* /sockjs/

To access websocket, please use URL:

* /sockjs/websocket

Simple example of usage present in test.html file.

Install
=======
To develop and run server you need:

 * Python 3.6 (Important, version should be <3.7)

First clone project:

```
git clone git@bitbucket.org:crowgroup/websocket_server.git
```

Now in project folder run:

```
pip install -r requirements.txt
```

Highly recommended to use virtualenv for developing with tools like pyenv, pipenv, poetry.


Deploy
======
Deployment should be done through docker image. To build docker image, run:

```
make build
```

To push image run:

```
make push
```

Messages format
===============
All messages in protocol serialized with JSON.

Auth message
------------
To auth you websocket connection first send auth message:

Request message

```
    {
        "type": "authentication",
        "value": "token"
    }
```

Response message

```
    {
        "status": "OK"
    }
```

In case of error response message will be different

```
    {
        "status": "ERROR"
        "description": "actual error description"
    }

```

Possible errors are:

 * invalid token


Events subscription
-------------------

To subscribe for events for specific panel send subscribe message:

Request message

```
    {
        "type": "subscribe",
        "value": panel_id OR panel_mac
    }
```

Response message

```
    {
        "status": "OK"
    }
```

In case of error response message "status" field will contain "ERROR" value

Possible errors are:

 * not authenticated
 * already subscribed
 * no permission


Receiving events
----------------
If you subscribe on events from at least one panel, you will receive events from this panel. No modification
applying on these events.


Close socket
------------
To close socket send close message

Request message

```
    {
        "type": "close",
        "value": ""
    }
```

Or just close websocket ;)
